import numpy as np
from os import listdir
from os.path import isfile, join
import math
import pickle

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import pandas as pd

class ShowerClass():
    '''
    this class should be used for relatve PDE calculation
    '''

    print('created shower class')


    def __init__(self, shower_wavelength = 400, shower_intensity = 20, time_position = 500e-9, time_distribution_data = 5e-9, pe = False):

        self.intensity = shower_intensity
        self.time_ns = time_position
        self.time_distribution_data = time_distribution_data
        self.in_pe = pe
        self.wavelength = shower_wavelength
        self.gaus_time = False

        try:
            self.time_dist_data = np.loadtxt(self.time_distribution_data , delimiter=" ", comments='#')
            self.time_dist_data[:,0] = self.time_dist_data[:,0]*1.e-9
        except:
                self.gaus_time = True
                self.gaus_time_std = self.time_distribution_data
                print('wavelength : ', self.wavelength)

    def get_shower_timing(self, n_events):

        if self.gaus_time:
            return np.random.normal(self.time_ns, self.gaus_time_std, n_events)
        else:
            return self.time_ns + np.random.choice(self.time_dist_data[:, 0], n_events, p=self.time_dist_data[:, 1])

    def build_time_distribution_hist(self, n_events):

        data = self.get_shower_timing(n_events)
        try:
            events, bins, pt = plt.hist(data, bins=  self.time_ns + self.time_dist_data[:, 0])
            plt.grid()
            plt.yscale('log')
            plt.xscale('log')
        except:
            plt.hist(data, bins=100)
            plt.grid()




    def get_root_hist(self, file, hist_name):

        f1 = root.TFile(file)
        h1 = f1.Get(hist_name)

        bin_center = []
        bin_content = []
        # signal = {}

        for ibin in range(1, h1.GetNbinsX(), 1):
            bin_center.append(h1.GetXaxis().GetBinCenter(ibin))
            bin_content.append(h1.GetBinContent(ibin))

        signal = {'time_ns': bin_center,
                  'n_events': bin_content}

        return signal



