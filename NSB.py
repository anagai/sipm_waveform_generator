import numpy as np
from os import listdir
from os.path import isfile, join
import math
import pickle

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import pandas as pd

class NSBClass():
    '''
    this class should be used for relatve PDE calculation
    '''

    print('created NSB class')


    def __init__(self, wavelength, rate_MHz = 100, pe = False):

        self.rate_mhz = rate_MHz
        self.in_pe = pe

        try:
            nsb_data = np.loadtxt(wavelength, delimiter="          ", comments='#')
            self.wavelength = nsb_data[:, 0]
            self.intensity = nsb_data[:,1]
            self.f1d = interp1d(self.wavelength, self.intensity, kind='cubic')
            print('NSB :: NSB spectrum was read from file')
        except:
            self.wavelength = wavelength
            print('NSB :: of single {:} nm wavelength will be used'.format(wavelength))

    def get_nsb_integrated(self, f_pde, wl_range = [300, 1000]):

        wavelength = np.arange(wl_range[0], wl_range[1], 1)
        nsb_photons_integral = np.zeros(len(wavelength))
        nsb_pe_integral = np.zeros(len(wavelength))

        for point, i_wl in enumerate(wavelength):
            if point > 0:
                nsb_photons_integral[point] = nsb_photons_integral[point - 1] + (wavelength[point] - wavelength[point - 1]) * self.f1d(i_wl)
                nsb_pe_integral[point] = nsb_pe_integral[point - 1] + (wavelength[point] - wavelength[point - 1]) * self.f1d(i_wl) * f_pde(i_wl)

        self.wavelength_interpolated = wavelength
        self.nsb_photons_integrated = nsb_photons_integral
        self.nsb_pe_integrated = nsb_pe_integral

    def calculate_nsb_level(self, aperture, solid_angle, safe_factor):

        self.nsb_level_pe = self.nsb_pe_integrated[-1]*safe_factor*solid_angle*aperture