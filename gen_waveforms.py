import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import pickle

from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d

from SiPM import SiPMClass
from WaveformGenerator import WaveformClass
from NSB import NSBClass
from Shower import ShowerClass

#Create shower:
my_shower = ShowerClass(400, 20, 800e-9, 5e-9, pe = False)


#Create SiPM
pde_data = './SiPM/FbK_NUV-HD_3x3_40um_epoxy_relative.txt'

size_mm = 3
upixel_size_mm = 0.025
n_pixels = int(pow(size_mm/upixel_size_mm, 2))
print('N pixels : ',n_pixels)

my_sipm = SiPMClass('NUVHD-25um', pde = pde_data, npixels = n_pixels, p_xt = 0.1, dcr = size_mm*size_mm*660.e3, pde_norm_factor = 0.46, pde_norm_wavelength = 420, norm_pde = True)
my_sipm.generate_template(3e-9, 2e-9, 64e-9, sampling=0.2e-9, length=1e-6)

#Create NSB
my_nsb = NSBClass("/home/andrii/work/sipm_waveform_generator/Spectrums/Spectra_NSB_ref.txt", rate_MHz = 9.003, pe = True)

#Generate Waveforms:
gen_waveform = WaveformClass(my_sipm, my_shower, my_nsb, length = 1.e-6, increment = 1.e-9, baseline = 0., pe_amplitude =10)
gen_waveform.generate_waveforms(10000)

#save results
gen_waveform.save_data("NUV-HD_25um")