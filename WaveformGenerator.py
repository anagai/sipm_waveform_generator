import numpy as np
from os import listdir
from os.path import isfile, join
import math
import pickle

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import pandas as pd

from SiPM import SiPMClass
from NSB import NSBClass
from Shower import ShowerClass

class WaveformClass():
    '''
    this class should be used for relatve PDE calculation
    '''

    print('created waveform class')


    def __init__(self, sipm, shower, nsb, length = 1e-6, increment = 1.e-9, baseline = 0, pe_amplitude = 10):

        print('NSB rate {:} MHz'.format(nsb.rate_mhz))
        print('p.e. or photons {:}'.format(nsb.in_pe))

        self.n_waveforms = 1
        self.length = length
        self.increment = increment
        self.time = np.arange(0, self.length, self.increment)

        self.n_points = len(self.time)
        self.baseline = baseline
        self.pe_amplitude = pe_amplitude

        self.nsb = nsb
        self.sipm = sipm
        self.shower = shower
        self.get_shower_intensity()

        self.xtalk_array = np.zeros(self.n_points)

    def generate_waveforms(self, n_waveforms):

        for i_waveform in range(n_waveforms):
            if i_waveform % 100 == 0:
                print("waveform number :: {:}".format(i_waveform))
            self.create_waveform_pe(i_waveform, n_waveforms = n_waveforms)

        # add gain smearing amd electronics noise
        self.add_sipm_gain_smearing()

    def inisialize_data(self, n_waveforms):

        self.amplitude_pe_generated_noise = np.zeros((n_waveforms, self.n_points))
        self.amplitude_pe_generated = np.zeros((n_waveforms, self.n_points))
        self.amplitude_pe_shower_generated = np.zeros((n_waveforms, self.n_points))
        self.amplitude_pe_detected_no_xt = np.zeros((n_waveforms, self.n_points))
        self.amplitude_pe_detected_withoutnoise = np.zeros((n_waveforms, self.n_points))
        self.amplitude_pe_std = np.zeros((n_waveforms, self.n_points))
        self.amplitude_pe_detected = np.zeros((n_waveforms, self.n_points))
        self.xtalk_array = np.zeros((n_waveforms, self.n_points))
        self.amplitude_lsb = np.zeros((n_waveforms, self.n_points))
        self.amplitude_lsb_nonoise = np.zeros((n_waveforms, self.n_points))


    def create_waveform_pe(self, index, n_waveforms = 1):

        if index == 0:
            self.inisialize_data(n_waveforms)
            # average number of NSB photons per time bin:
            self.get_average_nsb_rate_per_time_bin()

        # average number of DCR photons per time bin:
        self.avarage_dcr = self.sipm.dcr*self.increment
        npe_noise = np.random.poisson(self.nsb.average_pe + self.avarage_dcr, self.n_points)
        self.amplitude_pe_generated_noise[index] = npe_noise

        # create shower
        self.generate_shower(index)

        # sum all contributions: DCR + NSB + Shower
        self.amplitude_pe_generated[index] = self.amplitude_pe_generated_noise[index] + self.amplitude_pe_shower_generated[index]

        # get number of detected pe due to device saturation and PDE
        self.amplitude_pe_detected_no_xt[index] = self.get_n_fired(self.amplitude_pe_generated[index], 1.)

        # generate x-talk:
        self.generate_xt(index)
        self.amplitude_pe_detected[index] = self.amplitude_pe_detected_no_xt[index] + self.xtalk_array[index]

        self.generate_signals(index)

    def add_sipm_gain_smearing(self):

        #self.amplitude_pe_std = np.sqrt(self.sipm.std_el*self.sipm.std_el
        #                                       + self.amplitude_pe_detected*self.sipm.std_gain*self.sipm.std_gain)
        #self.amplitude_pe_detected = np.random.normal(self.amplitude_pe_detected, self.amplitude_pe_std, size=None)

        self.amplitude_pe_std = self.pe_amplitude*np.sqrt(self.sipm.std_el * self.sipm.std_el
                                               + (self.amplitude_lsb/self.pe_amplitude)*self.sipm.std_gain*self.sipm.std_gain)
        self.amplitude_lsb_nonoise = self.amplitude_lsb
        self.amplitude_lsb = np.random.normal(self.amplitude_lsb, self.amplitude_pe_std, size=None)

    def generate_shower(self, index):

        self.amplitude_pe_shower_generated[index] = np.zeros(self.n_points)
        self.generate_shower_timing()
        for i_bin in range(self.shower.npe_generated[0]):
            shower_time_bin = int(self.shower.pe_time[i_bin]/self.increment)
            if shower_time_bin >= 0 and shower_time_bin < self.n_points:
                self.amplitude_pe_shower_generated[index, shower_time_bin] = self.amplitude_pe_shower_generated[index, shower_time_bin] + 1


    def generate_xt(self, index):

        n_pe_generated = int(np.sum(self.amplitude_pe_detected_no_xt[index]))
        n_random_xf = np.random.uniform(0, 1, n_pe_generated)
        #print(n_pe_generated)
        i_photon = 0

        for ibin, npe in enumerate(self.amplitude_pe_detected_no_xt[index]):
            npe = int(npe)
            if npe > 0:
                for i_npe in range(npe):
                    if n_random_xf[i_photon] <= self.sipm.pxt:
                        self.xtalk_array[index, ibin] = self.add_xt(self.xtalk_array[index, ibin])
                    i_photon = i_photon + 1

    def get_shower_intensity(self):

        if self.shower.in_pe:
            #print('shower in pe')
            self.shower.npe = self.shower.intensity
        else:
            self.shower.npe = self.sipm.pde['f1d']((self.shower.wavelength))*self.shower.intensity

    def generate_shower_timing(self):

        self.shower.npe_generated = np.random.poisson(self.shower.npe, 1)
        #self.shower.pe_time = np.random.normal(self.shower.time_ns, self.shower.time_std, self.shower.npe_generated)
        self.shower.pe_time = self.shower.get_shower_timing(self.shower.npe_generated)

    def generate_signals(self, index):

        self.amplitude_lsb[index] = np.zeros(self.n_points)
        template_length = int(self.sipm.template['time'][-1]/self.increment) - 1

        n_signals_to_generate = int(np.sum(self.amplitude_pe_detected[index]))
        ramdom_time_shift = np.random.uniform(-0.5, 0.5, self.n_points)

        for i_time_bin in range(self.n_points):
            if self.amplitude_pe_detected[index, i_time_bin] > 0:
                n_end = i_time_bin + template_length
                if n_end > self.n_points:
                    n_end = self.n_points
                for i_point in range(i_time_bin, n_end):
                    template_amplitude = self.sipm.template['f1d']( self.increment*(i_point - i_time_bin + 1 + ramdom_time_shift[i_time_bin]) )
                    self.amplitude_lsb[index, i_point] = self.amplitude_lsb[index, i_point] + self.pe_amplitude*self.amplitude_pe_detected[index, i_time_bin]*template_amplitude


    def get_n_fired(self, Ngamma, PDE):

        n_fired = (self.sipm.n_pixels) * (1. - np.exp(-Ngamma * PDE / self.sipm.n_pixels))
        n_f_separated = divmod(n_fired, 1)
        n_f_separated_quotient = n_f_separated[0]
        n_f_separated_remainder = n_f_separated[1]

        n_f_random = np.random.uniform(0, 1, len(n_fired))

        n_detected = n_f_random < n_f_separated_remainder
        n_fired = n_f_separated_quotient + 1. * (n_detected)

        return n_fired.astype(int)

    def add_xt(self, n_xt):
        n_xt = n_xt + 1
        if np.random.uniform(0, 1, 1) <= self.sipm.pxt:
            n_xt = self.add_xt(n_xt)
        return n_xt

    def get_average_nsb_rate_per_time_bin(self):

        if self.nsb.in_pe == True:
            self.nsb.average_pe = self.increment*self.nsb.rate_mhz*1.e6
            print('average NSB per time bin : ', self.increment*self.nsb.rate_mhz*1.e6)

    def save_data(self, name):

        try:
            self.save_name = name + "_waveforms_data.pickle"
            with open(self.save_name, "wb") as f:
                pickle.dump(self, f, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as ex:
            print("Error during pickling object (Possibly unsupported):", ex)

    def load_object(self, file_name):
        try:
            with open(file_name, "rb") as f:
                self = pickle.load(f)
        except Exception as ex:
            print("Error during unpickling object (Possibly unsupported):", ex)

    
