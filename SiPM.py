import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import math
import pickle

from scipy.misc import electrocardiogram
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.interpolate import interp1d

class SiPMClass():

    print('created SiPM class')

    def __init__(self, simp_name, pde, npixels = 1600, p_xt = 0.1, dcr = 2e6, std_el = 0.1, std_gain = 0.2, pde_norm_factor = 1., pde_norm_wavelength = 400, norm_pde = False):

        self.name = simp_name
        self.n_pixels = npixels
        self.pxt = p_xt
        self.dcr = dcr
        self.pde = {}
        self.std_el = std_el
        self.std_gain = std_gain
        self.pde_norm_factor = pde_norm_factor
        self.pde_norm_wavelength = pde_norm_wavelength

        self.template = {}
        
        try:
            pde_data = np.loadtxt(pde, delimiter=" ", comments='#')
            self.pde['wavelength'] = pde_data[:, 0]
            self.pde['wavelength_err'] = pde_data[:, 1]

            if norm_pde:
                pde_f1 = interp1d(pde_data[:,0], pde_data[:,2], kind='cubic')
                self.pde['pde'] =  self.pde_norm_factor * pde_data[:, 2] / (pde_f1(self.pde_norm_wavelength))
                self.pde['pde_err'] =  self.pde_norm_factor * pde_data[:, 3] / (pde_f1(self.pde_norm_wavelength))
            else:
                self.pde['pde'] = pde_data[:,2]
                self.pde['pde_err'] = pde_data[:,3]

            #self.f_pde = interp1d(self.pde[:, 0], self.pde[:, 2], kind='cubic')
            if self.pde['wavelength'][-1] < 1000:
                self.pde['f1d'] = interp1d(self.pde['wavelength'], self.pde['pde'], fill_value = 'extrapolate')
            else:
                self.pde['f1d'] = interp1d(self.pde['wavelength'], self.pde['pde'], kind='cubic')

        except:
            print("Error : could not read PDE file")

    def read_template(self, template, sampling):

        try:
            template_tmp = np.loadtxt(template, delimiter=" ", comments='#')

            self.template['time'] = template_tmp[:,0]*sampling
            self.template['n_points'] = len(self.template['time'])
            self.template['amplitude'] = template_tmp[:,1]

            self.template['f1d'] = interp1d(self.template['time'], self.template['amplitude'], kind='cubic')
            print('SiPM :: Template was read from file')
        except:
            print("Error : could not read Template file : {:}".format(template))

    def describe(self):

        print("=======================")
        print("SiPM name : ", self.name)
        print("=======================")
        print("PDE {:} @ {:} nm".format(self.pde_norm_factor, self.pde_norm_wavelength, ))
        print("DCR {:.2f} MHz".format(self.dcr/1.e6))
        print("Pxt ", self.pxt)
        print("STD electronics: ", self.std_el)
        print("STD Gain: ", self.std_gain)
        print("=======================")


    def generate_template(self, rise_time_ns, fast_recovery, slow_recovery, sampling=0.2e-9, length=1e-6):

        self.template['time'] = np.arange(0, length, sampling)
        self.template['n_points'] = len(self.template['time'])
        self.template['amplitude'] = np.zeros(self.template['n_points'])

        # rise time:
        y_rise = np.arange(0, 1, sampling / rise_time_ns)

        # fast recovery:
        x_exp = np.arange(rise_time_ns, length, sampling)
        y_fast_recovery = 0.5*np.exp(-(x_exp - rise_time_ns) / fast_recovery)

        # slow recovery:
        x_exp = np.arange(rise_time_ns, length, sampling)
        y_slow_recovery = 0.5*np.exp(-(x_exp - rise_time_ns) / slow_recovery)

        for i in range(self.template['n_points']):
            if i < len(y_rise):
                self.template['amplitude'][i] = y_rise[i]
            else:
                self.template['amplitude'][i] = y_fast_recovery[i - len(y_rise)] + y_slow_recovery[i - len(y_rise)]

        #self.template['amplitude'] = self.template['amplitude'] / np.max(self.template['amplitude'])
        self.template['f1d'] = interp1d(self.template['time'], self.template['amplitude'], kind='cubic')

        print('SiPM :: Template was created')


    def save_data(self):

        try:
            self.save_name = self.name + "_data.pickle"
            with open(self.save_name, "wb") as f:
                pickle.dump(self, f, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as ex:
            print("Error during pickling object (Possibly unsupported):", ex)

    def load_object(self, file_name):
        try:
            with open(file_name, "rb") as f:
                self = pickle.load(f)
        except Exception as ex:
            print("Error during unpickling object (Possibly unsupported):", ex)

